from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest

# Create your tests here.
class Story9UnitTest(TestCase):
    def test_story_9_url_login_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_story_9_url_is_not_exist(self):
        response = self.client.get('/login/')
        self.assertFalse(response.status_code==404)
    
    def test_story_9_login_user_exists(self):
        response = self.client.get('/login/')

        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')

    def test_story_9_register_user_exists(self):
        response = self.client.get('/register/')

        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')
        self.assertContains(response, 'Password confirmation')

    def test_register_user(self):
        response = self.client.post('/', follow = True, data={
            'username': 'hilmeenn',
            'password1': 'halo123',
            'password2': 'halo123',
        })