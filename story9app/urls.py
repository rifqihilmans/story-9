from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView

app_name = 'story9app'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('login/', LoginView.as_view(), name='login'),
    path('register/', views.register, name='register'),
    path('logout/', LogoutView.as_view(next_page = 'story9app:dashboard'), name='logout'),
]